package com.example.rmstp3;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.os.Handler;
import android.telephony.*;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import java.io.*;
import java.net.*;
import java.nio.charset.Charset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String DEFAULT_TEXT = "Waiting ...";

    private Context context;
    private Button button;

    private String rttText = DEFAULT_TEXT;
    private String bandwidthText = DEFAULT_TEXT;
    private String jitterText = DEFAULT_TEXT;
    private String log_content = "";

    private String rttToCSV = "";
    private String bandwidthToCSV = "";
    private String jitterToCSV = "";
    private String wifiRSSIToCSV = "";
    private String cellularRSSIToCSV = "";
    private String cellularRSSITecToCSV = "";
    private String cellularISPToCSV = "";
    private int iterationValue = 1;

    private final List<Integer> rttValues = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = getApplicationContext();
        final TextView wifiRssiTextView = findViewById(R.id.rssiMessage);
        final TextView delayTextView = findViewById(R.id.delayMessage);
        final TextView cellularRssiTextView = findViewById(R.id.cellular_rssi);
        final TextView bandwidthTextView = findViewById(R.id.bandwidth);
        final TextView jitterTextView = findViewById(R.id.jitter);
        final TextView iterationTextView = findViewById(R.id.iter);
        button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Copy CSV", log_content);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(MainActivity.this, "Copied!", Toast.LENGTH_LONG).show();
            }
        });

        initialContentToFile();
        final Handler handler = new Handler();
        final Runnable r = new Runnable() {
            public void run() {
                handler.postDelayed(this, 3000);

                String rssi = calculateWifiRssi();
                wifiRssiTextView.setText("Wifi RSSI: " + rssi);

                calculateRtt();
                delayTextView.setText("RTT: " + rttText);

                String cellular = calculateCellularRssi(context);
                cellularRssiTextView.setText("Cellular RSSI: " + cellular);

                calculateBandwidth();
                bandwidthTextView.setText("BandWidth (Download): " + bandwidthText);

                if (rttValues.size() < 10) {
                    jitterTextView.setText("Jitter: " + jitterText);
                } else {
                    calculateJitter();
                    rttValues.remove(0);
                    jitterTextView.setText("Jitter: " + jitterText + " ms");
                }
                contentToFile();
                iterationTextView.setText("Iteration: " + iterationValue + " | " + (iterationValue - 1) * 3 + " sec");
                iterationValue++;
            }
        };
        handler.postDelayed(r, 1000);
    }


    private String calculateWifiRssi() {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        if (wifiInfo.getRssi() < -100) {
            return "Not in use";
        }
        wifiRSSIToCSV = String.valueOf(wifiInfo.getRssi());
        return wifiInfo.getRssi() + " dBm";
    }

    private String calculateCellularRssi(Context context) throws SecurityException {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String strength = "";
        List<CellInfo> cellInfos = telephonyManager.getAllCellInfo();   //This will give info of all sims present inside your mobile
        if (cellInfos != null) {
            for (int i = 0; i < cellInfos.size(); i++) {
                String isp = cellInfos.get(i).getCellIdentity().getOperatorAlphaShort().toString();
                if (cellInfos.get(i).isRegistered()) {
                    if (cellInfos.get(i) instanceof CellInfoWcdma) {
                        CellInfoWcdma cellInfoWcdma = (CellInfoWcdma) cellInfos.get(i);
                        CellSignalStrengthWcdma cellSignalStrengthWcdma = cellInfoWcdma.getCellSignalStrength();
                        strength = isp + " | Wcdma | " + cellSignalStrengthWcdma.getDbm() + " dBm";
                        cellularRSSIToCSV = String.valueOf(cellSignalStrengthWcdma.getDbm());
                        cellularISPToCSV = isp;
                        cellularRSSITecToCSV = "Wcdma";
                    } else if (cellInfos.get(i) instanceof CellInfoGsm) {
                        CellInfoGsm cellInfogsm = (CellInfoGsm) cellInfos.get(i);
                        CellSignalStrengthGsm cellSignalStrengthGsm = cellInfogsm.getCellSignalStrength();
                        strength = isp + " | Gsm | " + cellSignalStrengthGsm.getDbm() + " dBm";
                        cellularRSSIToCSV = String.valueOf(cellSignalStrengthGsm.getDbm());
                        cellularISPToCSV = isp;
                        cellularRSSITecToCSV = "Gsm";
                    } else if (cellInfos.get(i) instanceof CellInfoLte) {
                        CellInfoLte cellInfoLte = (CellInfoLte) cellInfos.get(i);
                        CellSignalStrengthLte cellSignalStrengthLte = cellInfoLte.getCellSignalStrength();
                        strength = isp + " | Lte | " + cellSignalStrengthLte.getDbm() + " dBm";
                        cellularRSSIToCSV = String.valueOf(cellSignalStrengthLte.getDbm());
                        cellularISPToCSV = isp;
                        cellularRSSITecToCSV = "Lte";
                    } else if (cellInfos.get(i) instanceof CellInfoCdma) {
                        CellInfoCdma cellInfoCdma = (CellInfoCdma) cellInfos.get(i);
                        CellSignalStrengthCdma cellSignalStrengthCdma = cellInfoCdma.getCellSignalStrength();
                        strength = isp + " | Cdma | " + cellSignalStrengthCdma.getDbm() + " dBm";
                        cellularRSSIToCSV = String.valueOf(cellSignalStrengthCdma.getDbm());
                        cellularISPToCSV = isp;
                        cellularRSSITecToCSV = "Cdma";
                    }
                }
            }
        }
        return strength;
    }

    private void calculateRtt() {
        final Handler handler = new Handler();
        Thread thread = new Thread(() -> {
            try {
                InetAddress inetAddress = InetAddress.getByName("1.1.1.1");
                InetSocketAddress inetSocketAddress = new InetSocketAddress(inetAddress, 443);
                Socket socket = new Socket();

                long beforeTime = System.currentTimeMillis();
                socket.connect(inetSocketAddress);
                long afterTime = System.currentTimeMillis();

                long rtt = afterTime - beforeTime;
                rttValues.add((int) rtt);

                handler.post(() -> this.rttText = rtt + " ms to 1.1.1.1");
                rttToCSV = String.valueOf(rtt);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        thread.start();
    }

    private void calculateBandwidth() {
        final Handler handler = new Handler();
        Thread thread = new Thread(() -> {
            try {
                URL fileUrl = new URL("https://www.gitlab.com/JoaoCadavez/rms-tp3-2021/-/raw/master/test-file.txt");
                URLConnection connection = fileUrl.openConnection();

                long t0 = System.currentTimeMillis();
                connection.getInputStream();
                long t1 = System.currentTimeMillis();

                double deltaT = ((double) t1 - (double) t0) / 1000d;
                double bandwidth = 8000 / deltaT;

                handler.post(() -> {
                    if (bandwidth / 1000 < 1) {
                        bandwidthText = (int) bandwidth + " Kb/s";
                    } else if (bandwidth / 1000 > 1) {
                        bandwidthText = (int) bandwidth / 1000 + " Mb/s";
                    }
                });
                bandwidthToCSV = String.valueOf((int) bandwidth);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        thread.start();
    }

    private void calculateJitter() {
        double sum = 0;

        for (Integer rtt : rttValues) {
            sum += rtt;
        }

        double average = (sum / rttValues.size());
        double result = 0;

        for (Integer rtt : rttValues) {
            double dif = Math.abs(rtt - average);
            if (dif > result) {
                result = dif;
            }
        }

        jitterText = String.valueOf((int) (result / 2));
        jitterToCSV = String.valueOf((int) (result / 2));
    }

    public void contentToFile() {
        String comma = ";";
        //ZonedDateTime dateTime = ZonedDateTime.now();
        //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy_HH:mm:ss");
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String content = timeStamp + comma + jitterToCSV + comma + bandwidthToCSV + comma + wifiRSSIToCSV + comma + cellularRSSIToCSV + comma + cellularRSSITecToCSV + comma + cellularISPToCSV + comma + rttToCSV + "\n";
        log_content = log_content + content;
    }

    public void initialContentToFile() {
        String comma = ";";
        String content = "TimeStamp" + comma + "Jitter" + comma + "Bandwidth" + comma + "WiFi RSSI" + comma + "Cellular RSSI" + comma + "Cellular Technology" + comma + "Cellular ISP" + comma + "RTT" + "\n";
        log_content = log_content + content;
    }


}
